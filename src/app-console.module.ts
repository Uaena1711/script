import { Module } from '@nestjs/common';
import { ConsoleModule } from 'nestjs-console';
import { MODULES } from './modules';

@Module({
    imports: [ ConsoleModule, ...MODULES],
    providers: [],
    exports: [],
})
export class AppConsoleModule {}
