import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { SolanaConsole } from './solana-script.console';

@Module({
  providers: [SolanaConsole],
  controllers: [],
  imports: [
    HttpModule,
  ],
})
export class SolanaModule {}
