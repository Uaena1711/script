import { Inject, Injectable } from '@nestjs/common';
import { SCRIPT_ACTION, programAddress, LAUNCH_TOKEN_LOG, rpc } from './const';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { AxiosResponse } from '@nestjs/terminus/dist/health-indicator/http/axios.interfaces';

@Injectable()
export class SolanaScript {

  private httpService: HttpService
  
  constructor(
  ) {
    this.httpService = new HttpService();
  }

  private initTxHash;

  public async prepare() {
    this.initTxHash = await this.getNewtx();
  }
  public async start(): Promise<void> {
    while (true) {
      const action = await this.onTick();
      if (SCRIPT_ACTION.Stop === action) {
        await this.sleep(2000);
      }

      const postMetadata = {
        "jsonrpc": "2.0",
        "id": 1,
        "method": "getSignaturesForAddress",
        "params": [programAddress, { "until": this.initTxHash, "before":  action}]
      };
  
      const transactions = this.httpService.post(rpc, postMetadata);
      const res = ((await firstValueFrom(transactions)) as AxiosResponse).data;

      if(res.result) {
        const signatures = res.result.map(signature => signature.signature);
        await this.process(signatures);
        this.initTxHash = action;
      }

    }
  }

  protected async process(txHashs) {
    try {
      for(const txhash of txHashs) {
        const postData = {
          "jsonrpc": "2.0",
          "id": 1,
          "method": "getTransaction",
          "params": [
            txhash,
            { 
              "maxSupportedTransactionVersion": 0,
            }
          ]
        };
  
        const txData = this.httpService.post(rpc, postData);
  
        const res = ((await firstValueFrom(txData)) as AxiosResponse).data;
  
        const programLog = res.result?.meta?.logMessages;
  
        // The init token log will be the 6th element of log array
        if (!programLog|| programLog[5] !== LAUNCH_TOKEN_LOG) {
          console.log('This is not launch tx: ', txhash);
          return;
        }
  
        const tokenAddress = res.result.meta.postTokenBalances[0].mint;
        console.log("this is new token address: ",tokenAddress);

      }

    } catch (e) {
      console.log(e);
    }
  }

  protected async onTick() {
    const newSignature = await this.getNewtx();
    if (newSignature === this.initTxHash) return SCRIPT_ACTION.Stop;
    return newSignature;
  }

  protected async sleep(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  protected async getNewtx() {
    const postMetadata = {
      "jsonrpc": "2.0",
      "id": 1,
      "method": "getSignaturesForAddress",
      "params": [programAddress, { "limit": 1 }]
    };

    const transaction = this.httpService.post(rpc, postMetadata);
    const res = ((await firstValueFrom(transaction)) as AxiosResponse).data;

    return res.result[0].signature;
  }
}