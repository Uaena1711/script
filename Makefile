ifndef u
u:=root
endif

deploy:
	rsync -avhzL --delete \
				--no-perms --no-owner --no-group \
				--exclude .idea \
				--exclude .git \
				--exclude node_modules \
				--exclude .env \
				--exclude db \
				--exclude db_data \
				. $(u)@$(h):$(dir)
	ssh -o StrictHostKeyChecking=no $(u)@$(h) "cd $(dir); docker-compose up -d --build"
	ssh -o StrictHostKeyChecking=no $(u)@$(h) "source ~/.nvm/nvm.sh; cd $(dir); ./server.sh"

deploy-dev: 
	make deploy h=165.22.99.44 dir=/root/ar404_be
