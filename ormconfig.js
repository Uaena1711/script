const dotenv = require('dotenv').config();

module.exports = {
    type: 'mysql',
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    entities: ['dist/entities/*.entity.js'],
    migrationsTableName: 'custom_migration_table',
    migrations: ['dist/database/migrations/*.js'],
    seeds: ['dist/**/database/seeds/**/*.js'],
    cli: {
      migrationsDir: 'src/database/migrations',
      entitiesDir: 'src/entities',
    },
    logging: true,
    synchronize: false,
    timezone: 'Z',
    extra: {
      decimalNumbers: true
    }
};